#!/bin/bash

learning_rates=(0.005 0.001 0.0005 0.0001)
hidden_layers=(1000 750 500 250 100)

for lr in "${learning_rates[@]}"
do
	for i in "${!hidden_layers[@]}";
	do
		for j in "${hidden_layers[@]:$i}";
		do
			echo "${lr} ${hidden_layers[$i]} ${j}"
			python3 trainModel.py --model=point --lr=$lr --layers ${hidden_layers[$i]} ${j} --o="./pointwise_sweep/point_${hidden_layers[$i]}_${j}_${lr}.json" --epochs=20 --maxScore=4 --batchSize=512 --minDelta=0.001 --maxPatience=2
		done
	done
done

