import torch
from torch import nn, Tensor
import numpy as np
from math import isnan

from evaluate import ndcg_at_k

class Loss_Base(nn.Module):
    def __init__(self, device="cpu"):
        super(Loss_Base, self).__init__()
        self.device = device
        self.to(self.device)

    def forward(self, x, labels, sig_par, *args):
        pass

class C_Loss(Loss_Base):

    def __init__(self, device="cpu"):
        super(C_Loss, self).__init__(device)

    def subtract_outer(self, x, y):
        return x.reshape(-1, 1) - y

    def compare_outer(self, x, y):
        return (x.reshape(-1,1) == y).to(torch.int32)

    def squeeze(self, x):
        x = torch.squeeze(x)

        if len(x.shape) == 0:
            x = torch.unsqueeze(x, 0)

        return x

    def compute_pair_diffs(self, x, labels):
        x = self.squeeze(x)
        labels = self.squeeze(labels)

        s_diff = self.subtract_outer(x, x)

        S_ij = torch.sign(self.subtract_outer(labels, labels))

        return x, labels, s_diff, S_ij

    def getC(self, x, labels, sig_par, *args):
        _, _, s_diff, S_ij = self.compute_pair_diffs(x, labels)

        return 0.5 * sig_par * (1 - S_ij) * s_diff + torch.log(1 + torch.exp(-sig_par * s_diff))

    def getMask(self, docQueries, shape):
        return torch.tril(self.compare_outer(docQueries, docQueries)) * (1 - torch.eye(shape[0]).to(self.device))

    def forward(self, x, labels, sig_par, docQueries, batchSize):

        docQueries = Tensor(docQueries).to(self.device)

        C = self.getC(x, labels, sig_par)

        mask = self.getMask(docQueries, C.shape)

        return (mask*C).sum()/batchSize

class Lambda_Loss(C_Loss):

    def __init__(self, device="cpu"):
        super(Lambda_Loss, self).__init__(device)

    def get_lambdas(self, s_diff, S_ij, sig_par):
        return sig_par * (0.5 * (1 - S_ij) - 1 / (1 + torch.exp(sig_par * s_diff)))

    def forward(self, x, labels, sig_par, docQueries, batchSize):

        docQueries = Tensor(docQueries).to(self.device)

        x, _, s_diff, S_ij = self.compute_pair_diffs(x, labels)

        mask = self.getMask(docQueries, S_ij.shape)
        loss = (mask*self.get_lambdas(s_diff, S_ij, sig_par)).sum(axis=0)

        return -loss.detach().dot(x)/batchSize

class LambdaRank_Loss(Lambda_Loss):

    def __init__(self, data, score_type, device="cpu"):
        super(LambdaRank_Loss, self).__init__(device)
        self.data = data
        self.score_type = score_type

    def ndcg(self, labels, scores):
        random_i = np.random.permutation(
               np.arange(scores.shape[0])
             )

        labels = labels[random_i]
        scores = scores[random_i]

        sort_ind = np.argsort(scores)[::-1]
        sorted_labels = labels[sort_ind]
        ideal_labels = np.sort(labels)[::-1]

        ndcg_ret = ndcg_at_k(sorted_labels, ideal_labels, 0)
        if isnan(ndcg_ret):
            return 0
        return ndcg_ret

    def err(self, scores):
        p = 1
        err = 0
        r = (2**scores -1.) / 2**np.max(scores)

        for i in range(r.shape[0]):
            err += p * r[i] / (i + 1)
            p = p * (1 - r[i])

        return err

    def compute_score(self, labels, scores):
        if self.score_type == "ndcg":
            return self.ndcg(labels, scores)
        elif self.score_type == "err":
            return self.err(scores)

    def swap(self, values, x, y):
        temp = values[x]
        values[x] = values[y]
        values[y] = temp

        return values

    def forward(self, x, labels, sig_par, docQueries, *args):

        q_id = docQueries[0]

        target_ranking = np.asarray(self.data.train.query_labels(q_id))

        x, _, s_diff, S_ij = self.compute_pair_diffs(x, labels)

        loss = self.get_lambdas(s_diff, S_ij, sig_par)

        scores_clone = x.cpu().clone().detach().numpy()
        baseline_score = self.compute_score(target_ranking, scores_clone)

        irm_values = np.zeros(loss.shape)
        assert (irm_values.shape[0] == irm_values.shape[1])

        for r in range(irm_values.shape[0]):
            for c in range(r, irm_values.shape[1]):
                irm_values[r][c] = np.abs(baseline_score - self.compute_score(target_ranking, self.swap(scores_clone, r, c)))

        irm_values += np.tril(np.transpose(irm_values), k=-1)

        loss = (loss * torch.Tensor(irm_values).to(self.device)).sum(axis=0)

        return -loss.detach().dot(x)

class RankNetLoss(Loss_Base):

    def __init__(self, loss_type, score_type, device="cpu", data=None):
        super(RankNetLoss, self).__init__(device)
        self.loss_type = loss_type

        self.loss_fn = None
        print("Here", self.loss_type)
        if self.loss_type == "ranknet_slow":
            self.loss_fn = C_Loss(device)
        elif self.loss_type == "ranknet_fast":
            self.loss_fn = Lambda_Loss(device)
        elif self.loss_type == "lambdarank":
            self.loss_fn = LambdaRank_Loss(data=data, device=device, score_type=score_type)

        assert (self.loss_fn is not None)

    def forward(self, x, labels, sig_par, *args):
        return self.loss_fn.forward(x, labels, sig_par, *args)

        
