# Code

The main script is `trainModel.py`, this script runs training (and serializes) any model implemented in `pointwiseModel.py` and `pairwiseModel.py` (which uses different losses from `loss_classes.py`), each of which is a child class of `trainableModel` (from `trainableModel.py`).

Please run

```
python3 trainModel.py --help
```

To know what each parameter does. Through the use of `--model`, `--loss_type`, and `--score_type` one can specify which of the 5 models to use. Other parameters control early stopping, training, model hyperparameters, plotting, and serializing a model. 

Please always run models with --maxScore 4 to ensure that the output of pointwise is bound between 0 and 4 (as we ran out tests)
