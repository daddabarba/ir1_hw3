from trainableModel import Trainable
from torch import nn, Tensor
import numpy as np

from random import shuffle

from math import ceil

class ScalingLayer(nn.Module):

    def __init__(
            self,
            maxVal,
            minVal = 0,
            getSquishingF = lambda : nn.Sigmoid(),
            rangeSF = (0,1)
    ):

        super(ScalingLayer, self).__init__()

        self.maxVal = maxVal
        self.minVal = minVal
        self.minSF, self.maxSF = rangeSF

        self.squishingF = getSquishingF()

    def forward(self, x):

        squished = self.squishingF(x)
        rel = (squished-self.minSF)/(self.maxSF-self.minSF)

        return rel*(self.maxVal-self.minVal) + self.minVal

class getBatches:

    def __init__(self, datafold, batchSize):

        # Store data to be used
        self.datafold = datafold

        # Store batch size
        self.batchSize = batchSize

        # Init range of data which constitutes the batch
        self.batchStart, self.batchEnd = 0, batchSize

        # Initialize index mapping (to allow shuffling without modifying datasplit object)
        self.idx = np.arange(datafold.train.num_docs())

        # Shuffle data
        shuffle(self.idx)

    def __len__(self):
        return ceil(self.datafold.train.num_docs()/self.batchSize)

    def __iter__(self):
        return self

    def __next__(self):

        # Get range for current batch
        batch_range = self.idx[self.batchStart:self.batchEnd]

        # Extract input feature vectors
        X = Tensor(self.datafold.train.feature_matrix[batch_range, :])

        # Extract respective targets
        t = Tensor(self.datafold.train.label_vector[batch_range, None])

        self.batchStart += self.batchSize
        self.batchEnd += self.batchSize

        if self.batchStart >= self.datafold.train.num_docs():
            raise StopIteration

        return X, t

class PointWise(Trainable):
    """
    This class implements a PW LTR model in PyTorch.
    It handles the different layers and parameters of the model.
    """


    def __init__(
            self,
            n_features,
            n_hidden,
            get_af = lambda : nn.LeakyReLU(0.2),
            maxScore = None,
            minScore = 0,
            device="cpu"
    ):

        super(PointWise, self).__init__()

        self.layers = []

        in_hidden = n_features

	# Initialize list of hidden layer
        for i in range(len(n_hidden)):
            out_hidden = n_hidden[i] # get output size of hidden layer

            self.layers.append(nn.Linear(in_hidden, out_hidden))
            self.layers.append(get_af())

            in_hidden = out_hidden # output of previous layer is input of next layer

        # Initialize output layer (one real value per input)
        self.layers.append(nn.Linear(in_hidden, 1))

        # Add scaling for output if specified
        if maxScore is not None:
            self.layers.append(ScalingLayer(maxScore, minScore))

        # Construct model that calls layers one after the other
        self.layers = nn.Sequential(*self.layers)

        # Store number of hidden layers (for later use)
        self.n_hidden = len(n_hidden)

        # Init loss function
        self.lossFN = nn.MSELoss().to(device)

        # GPU support
        self.device = device
        self.to(device)

        # Set batcher
        self.getBatches = getBatches

    def forward(self, x):
        return [self.layers(x.to(self.device))]

    def loss(self, y, t, *args):
        return self.lossFN(y, t.to(self.device))
