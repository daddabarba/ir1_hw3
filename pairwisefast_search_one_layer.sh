#!/bin/bash

learning_rates=(0.005 0.001 0.0005 0.0001)
hidden_layers=(100 250 500 750 1000)

for lr in "${learning_rates[@]}"
do
	for layers in "${hidden_layers[@]}"
	do
		echo "${lr} ${layers}"
		python3 trainModel.py --model=pair --lr=$lr --layers=$layers --loss_type=ranknet_fast --o="./pairwise_sweep_fast/pair_${layers}_${lr}.json" --epochs=10 --minDelta=0.001 --maxPatience=2 --batchSize=6
	done
done

