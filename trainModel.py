from argparse import ArgumentParser

from pointwiseModel import PointWise
from pairwiseModel import RankNet, getQueries

from torch import cuda, save
from dataset import get_dataset

import json

def parseModel(args, data):

    if args.model.lower() == "point":
        return PointWise(
            n_features=data.num_features,
            n_hidden=args.layers,
            device=args.device,
            maxScore=args.maxScore,
            minScore=args.minScore
        )
    elif args.model.lower() == "pair":
        return RankNet(
            datafold=data,
            n_features=data.num_features,
            n_hidden=args.layers,
            device=args.device,
            sigmoid_par = args.sigmoid_par,
            loss_type=args.loss_type,
            maxScore=args.maxScore,
            minScore=args.minScore,
            batcher=getQueries,
            score_type=args.score_type
        )

    return None

def main(args):

    # Data Loading
    print("Loading data...", end="")
    dataset = get_dataset()
    data = dataset.get_data_folds()[0]
    data.read_data()
    print("done")

    # Initialize model
    print("Initializing model...", end="")
    model = parseModel(args, data)
    print("done")

    # Train model
    print("Starting training procedure")
    history = model.trainFold(
            data, 
            nEpochs=args.epochs, 
            batchSize=args.batchSize, 
            lr=args.lr, 
            metrics=args.metric, 
            minDelta=args.minDelta, 
            maxPatience=args.maxPatience, 
            eval_step=args.eval_step,
            plot=args.plot
        )

    # Store results
    with open(args.o, 'w') as f:
        json.dump(history, f)

    # Serialize model if specified
    if args.pretrained is not None:
        with open(args.pretrained, 'wb') as f:
            save(model, f)

if __name__ == "__main__":

    # Parse arguments
    parser = ArgumentParser()

    parser.add_argument("--model", type=str, default="Point", help="Which model to sue: Point, Pair, or List")
    parser.add_argument("--layers", type=int, nargs="+", default=1000, help="Space separated list of ints, one for each layer. Each int is number of nurons in layer")
    parser.add_argument("--pretrained", type=str, default=None, help="Name of file in which to serialize or from which to load (if already existing) a pretrained model")

    parser.add_argument("--o", type=str, default="history.json", help="Name of file in which to dump results")

    parser.add_argument("--batchSize", type=int, default=1, help="Batch size to use during training")
    parser.add_argument("--lr", type=float, default=0.25, help="Learning rate to use")
    parser.add_argument("--epochs", type=int, default=20, help="Number of epochs to run")

    parser.add_argument("--maxScore", type=int, default=None, help="Upper bound for output")
    parser.add_argument("--minScore", type=int, default=0, help="Lower bound for output (used only if maxScore is given)")

    parser.add_argument("--minDelta", type=float, default=0, help="Threshold to consider a delta between epochs metrics equal to 0")
    parser.add_argument("--maxPatience", type=int, default=2, help="Number of consecutive epochs for which a 0 delta in the metric is tolerated")

    parser.add_argument("--metric", type=lambda x : x.split(" "), default="ndcg", help="Metric (or a series of metrics) used to evaluate (and plot if specified) the model. The first one is used for the early stopping")

    parser.add_argument("--sigmoid_par", type=int, default=1, help="sigmoid_par argument for RankNet (used only if model is pair)")
    parser.add_argument("--loss_type", type=str, default="ranknet_slow", help="Argument for RankNet. Three choices \"ranknet_slow\", \"ranknet_fast\", or \"lambdarank\"")
    parser.add_argument("--score_type", type=str, default="ndcg", help="Score type for LambdaRank loss. Two choices \"ndcg\" or \"err\"")

    parser.add_argument("--eval_step", type=int, default=10, help="Number of batches after which to evalute")
    parser.add_argument("--plot", type=str, default=None, help="Title of plot (if not given plotting is not performed)")

    args = parser.parse_args()
    if type(args.layers) is int:
        args.layers = [args.layers]

    args.device = "cuda" if cuda.is_available() else "cpu"

    main(args)
