from torch import load as torchLoad, Tensor, squeeze
from numpy import linspace, histogram
from dataset import get_dataset

from loss_classes import LambdaRank_Loss

from argparse import ArgumentParser

from matplotlib import pyplot as plt

def loadData():

    dataset = get_dataset()
    data = dataset.get_data_folds()[0]
    data.read_data()

    return data

def loadModel(filename):
    with open(filename, 'rb') as f:
        return torchLoad(f)

def getERR(model, data, dataset):

    loss = LambdaRank_Loss(data, "err", device=model.device)

    scores = squeeze(model(Tensor(dataset.feature_matrix).to(model.device))[0].detach().cpu()).numpy()

    return loss.err(scores)

def plotScoreDist(model, data, bins=None):

    # Get scores from model
    scoresModel = squeeze(model(Tensor(data.feature_matrix).to(model.device))[0].detach().cpu())

    # Get actual scores
    scoresTrue = data.label_vector

    # Compute bins
    if bins is None:
        maxScore = scoresTrue.max()
        minScore = scoresTrue.min()

        bins = linspace(minScore-0.5, maxScore+0.5, maxScore-minScore+2)

    # Get distributions
    histSM, _ = histogram(scoresModel, bins=bins)
    histTrue, _ =  histogram(scoresTrue, bins=bins)

    x = (bins+0.5)[:-1]

    # Make histograms for true model scores

    plt.plot(x, histSM, label="model's scores")
    plt.plot(x, histTrue, label="true scores")

    plt.xlabel("Score", fontsize=20)
    plt.ylabel("Absolute Frequency", fontsize=20)

    plt.legend()

if __name__ == "__main__":

    parser = ArgumentParser()

    parser.add_argument("--model", type=str, help="Name of serialized model to load")

    args = parser.parse_args()

    model, data = loadModel(args.model), loadData()
