#!/bin/bash

learning_rates=(0.005 0.001 0.0005 0.0001)
hidden_layers=(100 250 500 750 1000)

for lr in "${learning_rates[@]}"
do
	for layers in "${hidden_layers[@]}"
	do
		python3 trainModel.py --model=point --lr=$lr --layers=$layers --o="./pointwise_sweep/point_${layers}_${lr}.json" --epochs=20 --maxScore=4 --batchSize=512 --minDelta=0.001 --maxPatience=2
	done
done

