from torch import nn, optim, Tensor
from evaluate import evaluate

import matplotlib.pyplot as plt
from tqdm import tqdm

class Trainable(nn.Module):

    class getBatches:
        pass

    def forward(self, x):
        pass

    def loss(self, y, t, *args):
        pass

    def evaluate(self, dataset):

        scoreVal = self(Tensor(dataset.feature_matrix).to(self.device))[0]

        results =  evaluate(dataset, scoreVal.cpu().detach().squeeze().numpy(), print_results=False)
        results["arr"] = results["relevant rank"] # why spaces....

        return results

    def trainFold(
            self,
            datafold,
            nEpochs,
            lr,
            batchSize=1,
            get_optim = lambda pars, lr : optim.Adam(pars, lr),
            metrics=["ndcg"],
            minDelta = 0,
            maxPatience = 2,
            eval_step=10,
            plot=False
    ):

        # Init list to store results in
        epoch_history = [0]*nEpochs

        if plot is not None:
            # batch history for plotting
            batch_history = {}
            for metric in metrics:
                batch_history[metric] = []

            err_history = []

        # Initialize optimizer
        optim = get_optim(self.parameters(), lr)

        # Initialize patience
        countPatience = 0

        # Start training loop
        epoch_bar = tqdm(range(nEpochs), desc="epochs")
        for epoch in epoch_bar:

            # Keep track of mean error
            meanErr = 0

            # Loop over batches
            batch_bar = tqdm(self.getBatches(datafold, batchSize), desc="batches")
            for batch, (X, t, *kwargs) in enumerate(batch_bar):

                # Predict scores for feature vectors
                output = self(X, *kwargs)

                # Compute error
                err = self.loss(*output, t, *kwargs)

                # Update mean error
                meanErr += err.cpu().detach().numpy()

                # Compute gradients and update weights
                optim.zero_grad()
                err.backward()
                optim.step()

                # Display values
                batch_bar.set_description("err: %.3f"%(meanErr/(batch+1)))
                batch_bar.refresh()

                if plot is not None and batch > 0 and batch % eval_step == 0:
                    batch_results = self.evaluate(datafold.validation)
                    for metric in metrics:
                        batch_history[metric].append(batch_results[metric][0])
                    tqdm.write("epoch: %d %s: %.3f"%(epoch, metrics[0], batch_history[metrics[0]][-1]))
                    err_history.append(meanErr/(batch + 1))

            # Get metrics on test set

            results = self.evaluate(datafold.validation)
            tqdm.write("Evaluation at end of epoch %d %s: %.3f"%(epoch, metrics[0], results[metrics[0]][0]))

            # Store results
            #results['meanLoss'] = meanErr/nBatches
            epoch_history[epoch] = results

            # Early stopping
            if epoch>0 and abs(epoch_history[epoch-1][metrics[0]][0] - epoch_history[epoch][metrics[0]][0]) < minDelta:
                countPatience += 1
            else:
                countPatience = 0

            if countPatience >= maxPatience:
                break

        if plot is not None:

            _, axs = plt.subplots(len(batch_history)+1, 1)

            x_axis = [x for x in range(len(err_history))]

            axs[0].plot(x_axis, err_history)
            for i, metric in enumerate(metrics):
                axs[i+1].plot(x_axis, batch_history[metric])

            axs[0].set_ylabel("Loss", fontsize=20)

            for i, metric in enumerate(batch_history):
                axs[i+1].set_ylabel(metric, fontsize=20)

            axs[-1].set_xlabel("Batch", fontsize=20)
            axs[0].set_title(plot, fontsize=20)

            plt.show()

        return epoch_history
