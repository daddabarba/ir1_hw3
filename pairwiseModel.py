import torch
import numpy as np
from torch import nn, optim, Tensor
from random import shuffle
from math import ceil

from dataset import get_dataset
from pointwiseModel import PointWise
from loss_classes import RankNetLoss

class getQueries:

    def __init__(self, datafold, batchSize):

        # Store data to be used
        self.datafold = datafold

        self.batchSize = batchSize

        # Init query index
        self.q_idx = 0

        # Initialize index mapping (to allow shuffling without modifying datasplit object)
        self.idx = np.arange(datafold.train.num_queries())

        # Shuffle data
        shuffle(self.idx)

    def __len__(self):
        return ceil(self.datafold.train.num_queries()/self.batchSize)

    def __iter__(self):
        return self

    def __next__(self):

        X = None
        t = None
        docQueries = []

        for i in range(self.batchSize):

            # Get range for current batch
            query_id = self.idx[self.q_idx]

            x_part = self.datafold.train.query_feat(query_id)
            t_part = self.datafold.train.query_labels(query_id)

            if X is not None:
                X = np.vstack((X, x_part))
                t = np.hstack((t, t_part))
            else:
                X = x_part
                t = t_part

            docQueries += [query_id] * x_part.shape[0]

            # Update query index
            self.q_idx += 1

            if self.q_idx >= self.datafold.train.num_queries():
                raise StopIteration

        return Tensor(X), Tensor(t), docQueries, self.batchSize

class RankNet(PointWise):

    def __init__(
            self,
            datafold,
            n_features,
            n_hidden,
            score_type,
            sigmoid_par = 1,
            get_af = lambda : nn.LeakyReLU(0.2),
            loss_type="c",
            batcher = getQueries,
            maxScore = None,
            minScore = 0,
            device="cpu"
    ):
        super().__init__(n_features=n_features, n_hidden=n_hidden, get_af=get_af, maxScore=maxScore, minScore=minScore, device=device)

        self.datafold = datafold

        # Init loss function
        self.lossFN = RankNetLoss(loss_type=loss_type, device=device, data=datafold, score_type=score_type)

        self.sigmoid_par = sigmoid_par
        self.to(self.device)

        # Store batch iterator
        self.getBatches = batcher

    def loss(self, y, t, *args):
        return self.lossFN(y, t.to(self.device), self.sigmoid_par, *args)

    def forward(self, X, *args):
        return super(RankNet, self).forward(X)

if __name__ == "__main__":

    print("Loading data...", end="")
    dataset = get_dataset()
    data = dataset.get_data_folds()[0]
    data.read_data()
    print("done")

    print("Initializing model...", end="")
    model = RankNet(data.num_features, [1000], device="cuda:0")
    print("done")

    model.trainFold(data, 10, 0.1)
