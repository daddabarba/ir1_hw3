#!/bin/bash

learning_rates=(0.005 0.001 0.0005 0.0001)
hidden_layers=(100 250 500 750 1000)

for lr in "${learning_rates[@]}"
do
	for layers in "${hidden_layers[@]}"
	do
		python3 trainModel.py --model=pair --lr=$lr --layers=$layers --o="./lambdarank_err_sweep/point_${layers}_${lr}.json" --epochs=20 --maxScore=4 --minDelta=0.002 --maxPatience=2 --loss_type lambdarank --score_type err
	done
done

