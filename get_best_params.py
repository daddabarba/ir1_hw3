import os
import json

from argparse import ArgumentParser

import numpy as np

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--dir", type=str, default="./pointwise_sweep", help="Dir of json files")
    args = parser.parse_args()

    best_score = 0.0
    best_param = None
    for file_name in os.listdir(args.dir):
        print(file_name)

        with open(args.dir + "/" + file_name, "r") as file:
            data = json.load(file)

        las_idx = np.max(np.nonzero(data))

        score = data[las_idx]["ndcg"][0]
        
        if score > best_score:
            best_score = score
            best_param = file_name

    print(best_score, best_param)


